function plot_argentina_bubble(g) {

        // AD 
        var cities_var = Object();
        d3.json("cities_agr.topo.json", function(er, data) {
           argentina_cities = topojson.feature(data, data.objects.cities_agr).features;
        });



        d3.json("cities_agr.topo.json", function(error, cities) {
            if (error) {
                console.log("error");
                return error;
            }
            
            var radii = get_radii_for_cities(argentina_cities);
            
            console.log(cities);
            
            g.append("g")
                .attr("class", "cities")
                .selectAll("circle")
                .data(topojson.feature(cities, cities.objects.cities_agr).features)
                .enter().append("circle")
                .attr("transform", function(d) {
                    console.log(d);
                    console.log(path.centroid(d));
                    return "translate(" + path.centroid(d) + ")";
                })
                .attr("class", "city_bubble")
                .attr("r", function(city) {
                    var key = city.geometry.coordinates[0] + " " + city.geometry.coordinates[1]; 
                    return radii[key];
                });
        });


}

