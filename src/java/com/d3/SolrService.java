/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.d3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.*;
import org.json.JSONArray;

/**
 *
 * @author Aditya
 */
public class SolrService extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String numrec;
        
        numrec=request.getParameter("num");
        
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        StringBuilder sb = new StringBuilder("");
        try {
            url = new URL("http://localhost:8983/solr/collection1/select?q=*:*&fl=LatLong&rows="+numrec+"&wt=json");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* BufferedReader bf=new BufferedReader(new FileReader("/arj.json"));
        
         while(true)
         {
         String s=bf.readLine();
         if(s==null)
         break;
         sb.append(s);
         }*/
        JSONObject result=new JSONObject();
        JSONArray resultar=new JSONArray();
        JSONObject jo = null;
        try {
            jo = new JSONObject(sb.toString());
            
            JSONObject  res=(JSONObject) jo.get("response");
            JSONArray ja=res.getJSONArray("docs");
            int len=ja.length();
            for(int i=0;i<len;i++)
            {
                JSONObject j=ja.getJSONObject(i);
                String[] latlong=j.getString("LatLong").split(",");
                
                JSONObject newjo=new JSONObject();
                newjo.put("type", "Point");
                
                JSONArray newar=new JSONArray();
                newar.put(Double.parseDouble(latlong[1]));
                newar.put(Double.parseDouble(latlong[0]));
                
                
                newjo.put("coordinates", newar);
                               
                resultar.put(newjo);
            }
                 
            
        result.put("type", "Feature");
        result.put("geometry", resultar);
            
        } catch (JSONException ex) {
            Logger.getLogger(SolrService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.print(result.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
